import logging
import sys
import requests
from http import HTTPStatus

from hanziconv import HanziConv


logger = logging.getLogger(__name__)


class WITAI:

    API = 'https://api.wit.ai/speech'

    def __init__(self, token):
        self.token = token

    def wavToText(self, wave_file):
        with open(wave_file, 'rb') as wav:
            resp = requests.post(
                url=self.API,
                data=wav.read(),
                headers={
                    'Authorization': 'Bearer {}'.format(self.token),
                    'Content-Type': 'audio/wav',
                },
            )
            try:
                resp.raise_for_status()
                text = resp.json()['_text']
                return HanziConv.toTraditional(text)
            except Exception as e:
                print(e)
            return ''


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('usage: witai.py wit_ai_token wave_file')
        sys.exit(1)

    wit = WITAI(sys.argv[1])
    print(wit.wavToText(sys.argv[2]))
