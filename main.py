import argparse
import flask
import logging
import os
import requests
import subprocess
import sys
import tempfile
from flask import Flask, request, abort, make_response
from http import HTTPStatus
PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.dirname(PATH))

from witai import WITAI


logger = logging.getLogger(__name__)
app = Flask(__name__)


token = os.getenv('WIT_AI_TOKEN')
if token is None:
    print('Please set env WIT_AI_TOKEN')
    exit(1)
wit = WITAI(token)


@app.route('/')
def hello():
    return 'Hello World'


@app.route('/stt', methods=['POST'])
def speech_to_text():
    data = request.get_data()
    if data is None:
        abort(HTTPStatus.BAD_REQUEST)

    logger.debug('data len={}'.format(len(data)))

    _, orig_file = tempfile.mkstemp()
    with open(orig_file, 'wb') as f:
        f.write(data)

    _, conv_file = tempfile.mkstemp(suffix='.wav')
    subprocess.check_call(
        'ffmpeg -y -i {} -ar 8000 -ac 1 {}'.format(orig_file, conv_file),
        shell=True,
    )

    return make_response(flask.json.dumps(
        {'text': wit.wavToText(conv_file)}, ensure_ascii=False))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='speech to text service')
    parser.add_argument(
        '--port',
        type=int,
        default=5566,
        help='port of service')
    parser.add_argument('--debug', action='store_true', help='print debug msg')
    args = parser.parse_args()

    app.run(host='0.0.0.0', debug=args.debug, port=args.port)
