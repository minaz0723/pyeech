FROM python:3.6.6-alpine3.8

RUN apk add --update ffmpeg
RUN pip install -U pip
RUN pip install -U flask hanziconv requests

RUN mkdir /pyeech
ADD ./*.py /pyeech/
WORKDIR /pyeech

ENTRYPOINT ["python", "main.py"]
